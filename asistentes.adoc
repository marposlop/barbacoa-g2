== ﻿Lista de asistentes

// Ordenados por orden alfabético
// Formato: Apellidos, Nombre

* Bazán Romero, José Antonio
* Casanueva Morato, Daniel
* Cortes Vazquez, Jaime
* Iglesias Dominguez, Javier
* Jiménez Romero, Jesús
* Juan Chico, Jorge
* Lobato Salas, Jesús
* Olabarrieta Eduardo, Iraia
* Posada Lopez, Maria Jose
* Vázquez Pérez, Antonio

=== Organización del transporte

// Si tienes vehículo, pon el número de plazas. Si no tienes, añádete a
// alguno de los vehículos existentes.

==== Moto de Jorge (2 plazas)

* Jorge Juan-Chico

==== Citroën DS de antonio (5 plazas)

* Antonio Vázquez Pérez 

==== Coche de Jaime (4 plazas)

* Jaime Cortes Vazquez
* Javier Iglesias Dominguez 

==== Renault 19 de Dani

* Casanueva Morato, Daniel

==== Coche de Jesús (4 plazas)

* Jiménez Romero Jesús

==== Coche de Jesús Lobato (5 plazas)

* Lobato Salas, Jesús
